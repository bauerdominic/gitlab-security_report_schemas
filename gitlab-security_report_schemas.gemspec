# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name = "gitlab-security_report_schemas"
  spec.version = `cat gem_version`
  spec.authors = ["GitLab"]
  spec.email = ["gitlab_rubygems@gitlab.com"]
  spec.license = "Nonstandard"

  spec.summary = "Ruby gem for GitLab security report JSON schemas"
  spec.homepage = "https://gitlab.com/gitlab-org/ruby/gems/gitlab-security_report_schemas"
  spec.required_ruby_version = ">= 2.7.0"

  spec.metadata["homepage_uri"] = spec.homepage

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(__dir__) do
    `git ls-files -z`.split("\x0").reject do |f|
      (f == __FILE__) || f.match(%r{\A(?:(?:bin|test|spec|features)/|\.(?:git|travis|circleci)|appveyor)})
    end
  end

  # Bundle the schemas into the gem
  spec.files += `find schemas -type f`.split("\n")

  spec.bindir = "exe"
  spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "activesupport", ">= 6", "< 8"
  spec.add_dependency "json_schemer", "~> 0.2.18"
end
