# frozen_string_literal: true

require "git"

module Gitlab
  module SecurityReportSchemas
    module Release
      # The release workflow that updates a GitLab repository's Gemfile
      # dependency on this gem to an existing version published on rubygems.org
      # and creates a merge request for the change.
      class Workflow
        DEPENDENCY = "gitlab-security_report_schemas"
        COMMIT_MESSAGE = "Bump `#{DEPENDENCY}' to v%s"
        BRANCH_NAME = "#{DEPENDENCY}-v%s"
        TARGET_BRANCH = "master"

        def self.execute(configuration = Gitlab::SecurityReportSchemas.configuration)
          new(configuration).execute
        end

        attr_reader :configuration

        def initialize(configuration)
          @configuration = configuration
        end

        def execute
          gitlab_repository # ensure repository is cloned

          update_gemfile!
          install_dependencies!

          issue_url = create_issue!

          puts "Successfully created Issue: #{issue_url}"
        end

        def patch
          gitlab_repository.diff.patch
        end

        def gemfile_path
          gitlab_repository_path.join("Gemfile")
        end

        def gem_version
          File.read(gem_version_file).strip
        end

        def branch_name
          BRANCH_NAME % gem_version
        end

        def commit_message
          COMMIT_MESSAGE % gem_version
        end

        def target_project_id
          configuration.issue_target_project_id
        end

        def access_token
          configuration.gitlab_issue_access_token
        end

        private

        def gitlab_repository
          @gitlab_repository ||= if File.exist?(gitlab_repository_path)
                                   Git.open(gitlab_repository_path)
                                 else
                                   Git.clone(configuration.gitlab_repository, gitlab_repository_path, depth: 1)
                                 end
        end

        def update_gemfile!
          Gitlab::SecurityReportSchemas::Release::Gemfile.update!(self)
        end

        def install_dependencies!
          bundle do
            install!
            checksum!
            verify_checksum!
          end
        end

        def bundle(&block)
          Dir.chdir(gitlab_repository_path) do
            Gitlab::SecurityReportSchemas::Release::Bundler.module_eval(&block)
          end
        end

        def create_issue!
          Gitlab::SecurityReportSchemas::Release::Issue.create!(self)
        end

        def gitlab_repository_path
          Gitlab::SecurityReportSchemas.root_path.join("tmp", "gitlab")
        end

        def gem_version_file
          Gitlab::SecurityReportSchemas.root_path.join("gem_version")
        end
      end
    end
  end
end
