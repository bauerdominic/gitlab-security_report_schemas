# frozen_string_literal: true

require "tempfile"

module Gitlab
  module SecurityReportSchemas
    module Release
      # Changes or adds a dependency version to a Gemfile.
      class Gemfile
        SUBSTITUTION_REGEX_TEMPLATE = %{gem ['"]%s['"], ['"](.*)['"]}
        FEATURE_CATEGORY = :vulnerability_management

        def self.update!(workflow)
          File.open(workflow.gemfile_path, "r+").tap do |file|
            new(file).rewrite!(workflow.class::DEPENDENCY, workflow.gem_version)
          end
        end

        attr_reader :file

        def initialize(file)
          @file = file
        end

        private_class_method :new

        def rewrite!(dependency, version)
          contents = file.read

          file.seek(0)
          file.truncate(0)
          file.puts(substituted_content(contents, dependency, version))
        end

        private

        def substituted_content(contents, dependency, version)
          regex = Regexp.new(SUBSTITUTION_REGEX_TEMPLATE % dependency)
          dep = "gem '#{dependency}', '#{version}'"

          return contents.gsub!(regex, dep) if contents.match(regex)

          contents.concat("#{dep}, feature_category: #{FEATURE_CATEGORY.inspect}")
          contents.concat("\n") unless contents.end_with?("\n")
        end
      end
    end
  end
end
