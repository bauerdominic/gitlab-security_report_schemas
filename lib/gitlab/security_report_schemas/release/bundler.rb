# frozen_string_literal: true

module Gitlab
  module SecurityReportSchemas
    module Release
      # Wrapper around bundle(1).
      module Bundler
        extend self

        def install!
          run("bundle install")
        end

        def checksum!
          run("bundle exec bundler-checksum init")
        end

        def verify_checksum!
          run("bundle exec bundler-checksum verify")
        end

        private

        def run(command)
          # Bundler modifies RUBYOPT and RUBYLIB which makes bundle(1) attempt
          # to load from our gem environment instead of from the GitLab environment.
          ::Bundler.with_unbundled_env do
            system(command) || raise("non-zero exit code: `#{command}`")
          end
        end
      end
    end
  end
end
