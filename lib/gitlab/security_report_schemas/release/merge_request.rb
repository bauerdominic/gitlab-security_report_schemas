# frozen_string_literal: true

require "net/http"
require "uri"
require "erb"
require "json"

module Gitlab
  module SecurityReportSchemas
    module Release
      # GitLab merge request creation.
      class MergeRequest
        API_ROOT = "https://gitlab.com/api/v4"
        DESCRIPTION_TEMPLATE = "mr_description.erb"

        def self.create!(workflow)
          new(workflow).execute!
        end

        attr_reader :workflow

        def initialize(workflow)
          @workflow = workflow
        end

        def execute!
          http = Net::HTTP.new(uri.host, uri.port)
          http.use_ssl = true

          case response = http.request(request)
          when Net::HTTPSuccess then extract_url(JSON.parse(response.body))
          else raise "Failed to create merge request: #{response.code} -- #{response.message}"
          end
        end

        def uri
          URI(File.join(API_ROOT, "projects", workflow.target_project_id.to_s, "merge_requests"))
        end

        def body
          {
            source_branch: workflow.branch_name,
            target_branch: workflow.class::TARGET_BRANCH,
            title: workflow.commit_message,
            description: description
          }
        end

        def headers
          {
            "Content-Type" => "application/json",
            "Authorization" => "Bearer #{workflow.access_token}"
          }
        end

        def description
          description_template.result_with_hash(gem_version: workflow.gem_version)
        end

        private

        def request
          Net::HTTP::Post.new(uri, headers).tap do |req|
            req.body = body.to_json
          end
        end

        def extract_url(body)
          body["web_url"]
        end

        def description_template
          ERB.new(File.read(description_template_path))
        end

        def description_template_path
          Gitlab::SecurityReportSchemas.root_path.join(
            "lib", "gitlab", "security_report_schemas", "release", "templates", DESCRIPTION_TEMPLATE
          )
        end
      end
    end
  end
end
