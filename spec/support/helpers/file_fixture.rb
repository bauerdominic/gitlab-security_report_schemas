# frozen_string_literal: true

module FileFixture
  def file_fixture(*fixture_name)
    path = Gitlab::SecurityReportSchemas.root_path.join("spec", "fixtures", *fixture_name)

    if path.exist?
      path
    else
      msg = "The fixture does not exist '%s'"

      raise ArgumentError, format(msg, path)
    end
  end
end
