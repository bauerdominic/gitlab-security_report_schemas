# rubocop:disable Style/FrozenStringLiteralComment
# Incompatible with `frozen_string_literal` since this test requires a mutable
# StringIO instance.

RSpec.describe Gitlab::SecurityReportSchemas::Release::Gemfile, "#rewrite!" do
  let(:file) { StringIO.new(contents) }
  subject(:gemfile) { described_class.send(:new, file) }

  after { file.close }

  context "with dependency present" do
    let(:contents) do
      <<~GEMFILE
        gem 'bcrypt', '~> 3.1', '>= 3.1.14' # rubocop:todo Gemfile/MissingFeatureCategory
        gem 'gitlab-security_report_schemas', '0.4.2.min1.2.3.max4.5.6', feature_category: :vulnerability_management
        gem 'graphql', '~> 1.13.19', feature_category: :api
        gem 'device_detector'
      GEMFILE
    end

    it "substitutes the version" do
      gemfile.rewrite!("gitlab-security_report_schemas", "0.0.1.min9.2.2.max8.3.3")

      expect(file.string).to eq <<~GEMFILE
        gem 'bcrypt', '~> 3.1', '>= 3.1.14' # rubocop:todo Gemfile/MissingFeatureCategory
        gem 'gitlab-security_report_schemas', '0.0.1.min9.2.2.max8.3.3', feature_category: :vulnerability_management
        gem 'graphql', '~> 1.13.19', feature_category: :api
        gem 'device_detector'
      GEMFILE
    end
  end

  context "with dependency absent" do
    let(:contents) do
      <<~GEMFILE
        gem 'bcrypt', '~> 3.1', '>= 3.1.14' # rubocop:todo Gemfile/MissingFeatureCategory
        gem 'graphql', '~> 1.13.19', feature_category: :api
        gem 'device_detector'
      GEMFILE
    end

    it "adds the dependency" do
      gemfile.rewrite!("gitlab-security_report_schemas", "0.0.1.min9.2.2.max8.3.3")

      expect(file.string).to eq <<~GEMFILE
        gem 'bcrypt', '~> 3.1', '>= 3.1.14' # rubocop:todo Gemfile/MissingFeatureCategory
        gem 'graphql', '~> 1.13.19', feature_category: :api
        gem 'device_detector'
        gem 'gitlab-security_report_schemas', '0.0.1.min9.2.2.max8.3.3', feature_category: :vulnerability_management
      GEMFILE
    end
  end
end
# rubocop:enable Style/FrozenStringLiteralComment
