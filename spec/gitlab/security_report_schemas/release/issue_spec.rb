# frozen_string_literal: true

RSpec.describe Gitlab::SecurityReportSchemas::Release::Issue do
  let(:target_project_id) { 123 }
  let(:access_token) { "foobar" }
  let(:workflow) { Gitlab::SecurityReportSchemas::Release::Workflow.new(configuration) }

  let(:configuration) do
    Gitlab::SecurityReportSchemas::Configuration.new do |config|
      config.issue_target_project_id = target_project_id
      config.gitlab_issue_access_token = access_token
    end
  end

  subject(:issue) { described_class.new(workflow) }

  describe "#uri" do
    subject { issue.uri.to_s }

    it { is_expected.to eq("https://gitlab.com/api/v4/projects/#{target_project_id}/issues") }
  end

  describe "#body" do
    subject(:body) { issue.body }
    let(:patch) { "patch" }

    before do
      allow(workflow).to receive(:patch).and_return(patch)
    end

    specify do
      is_expected.to include(title: workflow.commit_message)
    end

    describe "description" do
      subject { body[:description] }

      it { is_expected.to include(workflow.gem_version) }
      it { is_expected.to include(workflow.patch) }
    end
  end

  describe "#headers" do
    subject(:headers) { issue.headers }

    describe "access token" do
      subject { headers["Authorization"] }

      it { is_expected.to include(workflow.access_token) }
    end
  end
end
