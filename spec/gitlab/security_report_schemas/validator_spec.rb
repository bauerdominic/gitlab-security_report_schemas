# frozen_string_literal: true

RSpec.describe Gitlab::SecurityReportSchemas::Validator do
  let(:validator) { described_class.new({}, "sast", "0.1.0") }
  let(:validation_result) { [] }
  let(:supported_schema?) { false }
  let(:deprecated_schema?) { false }
  let(:fallback_schema?) { false }
  let(:schema_ver_version) { nil }
  let(:mock_schema) do
    instance_double(Gitlab::SecurityReportSchemas::Schema,
                    supported?: supported_schema?,
                    deprecated?: deprecated_schema?,
                    fallback?: fallback_schema?,
                    schema_ver_version: schema_ver_version,
                    validate: validation_result)
  end

  before do
    allow(validator).to receive(:schema).and_return(mock_schema)
  end

  describe "#valid?" do
    subject { validator.valid? }

    context "when the schema is not supported" do
      it { is_expected.to be_falsey }
    end

    context "when the schema is supported" do
      let(:supported_schema?) { true }

      context "when there are validation errors" do
        let(:validation_result) do
          [{ "type" => "required", "data_pointer" => "root", "details" => { "missing_keys" => ["foo"] } }]
        end

        it { is_expected.to be_falsey }
      end

      context "when there is no validation error" do
        it { is_expected.to be_truthy }
      end
    end
  end

  describe "#errors" do
    let(:schema_ver_version) { "0.1.0" }
    subject { validator.errors }

    context "when the schema is not supported" do
      before do
        allow(Gitlab::SecurityReportSchemas).to receive(:supported_versions).and_return(["0.2.0"])
      end

      let(:expected_error_message) do
        "Version 0.1.0 for report type sast is unsupported, " \
        "supported versions for this report type are: [\"0.2.0\"]"
      end

      it { is_expected.to match_array([expected_error_message]) }
    end

    context "when the schema is supported" do
      let(:supported_schema?) { true }

      context "when there are validation errors" do
        let(:validation_result) do
          [{ "type" => "required", "data_pointer" => "root", "details" => { "missing_keys" => ["foo"] } }]
        end

        it { is_expected.to eq(["property 'root' is missing required keys: foo"]) }
      end

      context "when there is no validation error" do
        let(:validation_result) { [] }

        it { is_expected.to be_empty }
      end
    end
  end

  describe "#warnings" do
    let(:schema_ver_version) { "0.1.1" }
    let(:expected_deprecation_message) do
      "Version 0.1.1 for report type sast has been deprecated, " \
      "supported versions for this report type are: [\"0.2.0\"]"
    end

    let(:expected_fallback_message) do
      "This report uses a supported MAJOR.MINOR version but the PATCH doesn't match " \
      "any vendored schema version. Validation is done against version 0.1.1"
    end

    subject { validator.warnings }

    context "when the schema is not deprecated" do
      context "when the fallback schema is not used" do
        it { is_expected.to be_empty }
      end

      context "when the fallback schema is used" do
        let(:fallback_schema?) { true }

        it { is_expected.to match_array([expected_fallback_message]) }
      end
    end

    context "when the schema is deprecated" do
      let(:deprecated_schema?) { true }

      before do
        allow(Gitlab::SecurityReportSchemas).to receive(:supported_versions).and_return(["0.2.0"])
      end

      context "when the fallback schema is not used" do
        it { is_expected.to match_array([expected_deprecation_message]) }
      end

      context "when the fallback schema is used" do
        let(:fallback_schema?) { true }

        it { is_expected.to match_array([expected_deprecation_message, expected_fallback_message]) }
      end
    end
  end
end
