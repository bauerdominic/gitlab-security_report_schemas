# frozen_string_literal: true

RSpec.describe Gitlab::SecurityReportSchemas::SchemaVer do
  describe ".new!" do
    let(:exact_schema_or_fallback) { instance_double(described_class) }
    let(:mock_instance) { instance_double(described_class, itself_or_fallback: exact_schema_or_fallback) }

    subject(:new!) { described_class.new!(version) }

    before do
      allow(described_class).to receive(:new).and_return(mock_instance)
    end

    context "when the given version is blank" do
      context "when the given version is empty string" do
        let(:version) { "" }

        it { is_expected.to eq(Gitlab::SecurityReportSchemas.supported_versions.last) }
      end

      context "when the given version is `nil`" do
        let(:version) { nil }

        it { is_expected.to eq(Gitlab::SecurityReportSchemas.supported_versions.last) }
      end
    end

    context "when the given version is not blank" do
      context "when the given version is malformed" do
        let(:version) { "14.0" }

        it "raises `InvalidSchemaVersion` error" do
          expect { new! }.to raise_error(described_class::InvalidSchemaVersion)
        end
      end

      context "when the given version is well-formed" do
        let(:version) { "13.0.0" }

        it { is_expected.to eq(exact_schema_or_fallback) }
      end

      context "when the given version is v-prefixed" do
        let(:version) { "v13.0.0" }

        it { is_expected.to eq(exact_schema_or_fallback) }
      end
    end
  end

  describe "#itself_or_fallback" do
    let(:schema_ver_4) { described_class.new("0.4.0") }

    subject(:fallback) { schema_ver.itself_or_fallback }

    before do
      allow(Gitlab::SecurityReportSchemas).to receive(:supported_versions).and_return([schema_ver_4])
    end

    context "when the schema_ver is already supported" do
      let(:schema_ver) { described_class.new("0.4.0") }

      it { is_expected.to eq(schema_ver) }

      it "does not set the `fallback_to` of the object" do
        expect(fallback.fallback_to).to be_nil
      end
    end

    context "when the schema_ver is not supported" do
      context "when there is a fallback" do
        let(:schema_ver) { described_class.new("0.4.1") }

        it { is_expected.to eq(schema_ver_4) }

        it "sets the `fallback_to` of the fallback object" do
          expect(fallback.fallback_to).to eq(schema_ver)
        end
      end

      context "when there is no fallback" do
        let(:schema_ver) { described_class.new("0.5.0") }

        it { is_expected.to eq(schema_ver) }

        it "does not set the `fallback_to` of the object" do
          expect(fallback.fallback_to).to be_nil
        end
      end
    end
  end

  describe "#supported?" do
    let(:schema_ver_4) { described_class.new("0.4.0") }
    let(:schema_ver_3) { described_class.new("0.3.0") }

    subject { schema_ver.supported? }

    before do
      allow(Gitlab::SecurityReportSchemas).to receive(:supported_versions).and_return([schema_ver_4])
    end

    context "when there is a schema for the given version" do
      let(:schema_ver) { described_class.new("0.4.0") }

      it { is_expected.to be_truthy }
    end

    context "when there is no schema for the given version" do
      let(:schema_ver) { described_class.new("0.3.0") }

      it { is_expected.to be_falsey }
    end
  end

  describe "#deprecated?" do
    subject { schema_ver.deprecated? }

    before do
      Gitlab::SecurityReportSchemas.configure do |config|
        config.deprecated_versions = ["0.1.0"]
      end
    end

    context "when the given version is deprecated" do
      let(:schema_ver) { described_class.new("0.1.0") }

      it { is_expected.to be_truthy }
    end

    context "when the given version is not deprecated" do
      let(:schema_ver) { described_class.new("0.2.0") }

      it { is_expected.to be_falsey }
    end
  end

  describe "#fallback?" do
    let(:schema_ver) { described_class.new("0.2.0") }

    subject { schema_ver.fallback? }

    context "when the schema_ver is set as a fallback to another schema_ver" do
      before do
        schema_ver.fallback_to = described_class.new("0.3.0")
      end

      it { is_expected.to be_truthy }
    end

    context "when the schema_ver is not set as a fallback to another schema_ver" do
      it { is_expected.to be_falsey }
    end
  end
end
