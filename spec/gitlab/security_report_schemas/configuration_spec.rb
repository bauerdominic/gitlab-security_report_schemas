# frozen_string_literal: true

RSpec.describe Gitlab::SecurityReportSchemas::Configuration do
  subject(:config) { described_class.new }
  let(:env) { {} }

  before do
    stub_const("ENV", env)
  end

  describe "value precedence" do
    subject(:value) { config.issue_target_project_id }

    context "with runtime and env var value" do
      it "returns runtime value" do
        config.issue_target_project_id = "123"
        ENV["ISSUE_TARGET_PROJECT_ID"] = "456"
        expect(value).to eq("123")
      end
    end

    context "with runtime value only" do
      it "returns runtime value" do
        config.issue_target_project_id = "123"
        expect(value).to eq("123")
      end
    end

    context "with env var value only" do
      it "returns env var value" do
        ENV["ISSUE_TARGET_PROJECT_ID"] = "456"
        expect(value).to eq("456")
      end
    end

    context "without runtime or env var value" do
      it "defaults" do
        expect(value).to eq("278964")
      end
    end
  end
end
