# frozen_string_literal: true

require "gitlab/security_report_schemas"
require "shoulda-matchers"

Dir[Gitlab::SecurityReportSchemas.root_path.join("spec/support/**/*.rb")].sort.each { |file| require file }

Shoulda::Matchers.configure do |config|
  config.integrate do |with|
    with.test_framework :rspec
  end
end

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = ".rspec_status"

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end

  config.before(:suite) do
    Gitlab::SecurityReportSchemas.configure do |conf|
      conf.schemas_path = Gitlab::SecurityReportSchemas.root_path.join("spec", "fixtures", "schemas")
    end
  end

  # Include custom helpers
  config.include FileFixture
end
